Student of Computer Science in University of Gdańsk. I passed the first year and I’m currently looking for opportunities to gain some experience and learn about practical applications of the tools I studied.

:triangular_flag_on_post: Gdańsk\
:email: ubartek9@gmail.com\
[Linkedin](https://www.linkedin.com/in/bartosz-urbanowicz-6722a8268/)